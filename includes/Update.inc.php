<?php
/**
 * Update
 *
 * @package Marking Period Groups module
 */

$copy_to_getmp_file = 'functions/1GetMP.php';

// Copy 1GetMP.php to functions/
$copy_getmp_file = 'modules/Marking_Period_Groups/functions/1GetMP.php';

if ( file_exists( $copy_getmp_file )
	&& ( ! file_exists( $copy_to_getmp_file )
		|| filesize( $copy_to_getmp_file ) !== filesize( $copy_getmp_file ) ) )
{
	if ( version_compare( '11.1', ROSARIO_VERSION, '>' ) )
	{
		echo ErrorMessage( [ 'This module requires RosarioSIS version 11.1 or higher. Please upgrade.' ], 'fatal' );
	}
	elseif ( ! is_writable( 'functions/' )
		|| ! copy( $copy_getmp_file, $copy_to_getmp_file ) )
	{
		echo ErrorMessage( [ sprintf(
			dgettext( 'Marking_Period_Groups', 'Please copy the %s file and place it inside the functions/ directory.' ),
			$copy_getmp_file
		) ] );

		require_once $copy_getmp_file;
	}
}
