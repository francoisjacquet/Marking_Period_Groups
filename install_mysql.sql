/**
 * Install MySQL
 * Required if the module adds programs to other modules
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Marking Period Groups module
 */

/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Marking_Period_Groups/MarkingPeriods.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Marking_Period_Groups/MarkingPeriods.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Marking_Period_Groups/GradeLevels.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Marking_Period_Groups/GradeLevels.php'
    AND profile_id=1);


--
-- Name: school_marking_period_groups; Type: TABLE;
--

CREATE TABLE IF NOT EXISTS school_marking_period_groups (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    syear numeric(4,0) NOT NULL,
    school_id integer NOT NULL,
    title text NOT NULL,
    rollover_id integer,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp,
    FOREIGN KEY (school_id,syear) REFERENCES schools(id,syear)
);


--
-- Name: school_marking_period_groupxmp; Type: TABLE;
--

CREATE TABLE IF NOT EXISTS school_marking_period_groupxmp (
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES school_marking_period_groups(id),
    marking_period_id int NOT NULL,
    FOREIGN KEY (marking_period_id) REFERENCES school_marking_periods(marking_period_id) ON DELETE CASCADE,
    UNIQUE (group_id, marking_period_id)
);


--
-- Name: school_marking_period_groupxgradelevel; Type: TABLE;
--

CREATE TABLE IF NOT EXISTS school_marking_period_groupxgradelevel (
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES school_marking_period_groups(id),
    grade_level_id int NOT NULL,
    FOREIGN KEY (grade_level_id) REFERENCES school_gradelevels(id) ON DELETE CASCADE,
    UNIQUE (group_id, grade_level_id)
);
