<?php
/**
 * Functions
 *
 * @package Marking Period Groups module
 */

if ( isset( $_REQUEST['sidefunc'] )
	&& $_REQUEST['sidefunc'] === 'update'
	&& $_POST )
{
	// Update Admin & Teachers's current School.
	if ( ( User( 'PROFILE' ) === 'admin'
			|| User( 'PROFILE' ) === 'teacher' )
		&& isset( $_REQUEST['school'] )
		&& $_REQUEST['school'] != UserSchool() )
	{
		// Reset current MP Group.
		$_SESSION['UserMPGroup'] = '0';
	}

	// Update current SchoolYear.
	elseif ( isset( $_REQUEST['syear'] )
		&& $_REQUEST['syear'] != UserSyear() )
	{
		// Reset current MP Group.
		$_SESSION['UserMPGroup'] = '0';
	}
}

/**
 * Set User Marking Period Group depending of Profile
 * For students / parents, MP Group is defined by student's Grade Level
 *
 * Called at the start of SideMarkingPeriodSelect()
 * so Group is set before getting Marking Periods
 *
 * @global $_SESSION['UserMPGroup']
 */
function SetUserMPGroup()
{
	if ( ! function_exists( 'UserMPGroup' ) )
	{
		return;
	}

	if ( is_null( UserMPGroup() ) )
	{
		$_SESSION['UserMPGroup'] = '0';
	}

	if ( User( 'PROFILE' ) === 'admin'
		|| User( 'PROFILE' ) === 'teacher' )
	{
		if ( UserMP() )
		{
			$_SESSION['UserMPGroup'] = (int) GetMPGroup( UserMP() );
		}

		return;
	}

	if ( ! UserStudentID() )
	{
		return;
	}

	$student_grade_level = DBGetOne( "SELECT GRADE_ID
		FROM student_enrollment
		WHERE STUDENT_ID='" . UserStudentID() . "'
		AND SCHOOL_ID='" . UserSchool() . "'
		AND SYEAR='" . UserSyear() . "'
		ORDER BY START_DATE DESC
		LIMIT 1" );

	if ( ! $student_grade_level )
	{
		return;
	}

	$_SESSION['UserMPGroup'] = (int) DBGetOne( "SELECT GROUP_ID
		FROM school_marking_period_groupxgradelevel
		WHERE GRADE_LEVEL_ID='" . (int) $student_grade_level . "'
		AND GROUP_ID IN (SELECT ID
			FROM school_marking_period_groups
			WHERE SYEAR='" . UserSyear() . "'
			AND SCHOOL_ID='" . UserSchool() . "')" );
}


if ( ! function_exists( 'SideMarkingPeriodSelect' )
	&& function_exists( 'UserMPGroup' ) ) :
/**
 * Marking Period Select Input
 * Reset UserMP if invalid
 *
 * Override this function in your add-on functions.php file
 *
 * @since RosarioSIS 11.1
 *
 * @return string HTML Select Input
 */
function SideMarkingPeriodSelect()
{
	SetUserMPGroup();

	$mp_RET = $other_groups_RET = [];

	if ( User( 'PROFILE' ) !== 'admin'
		&& User( 'PROFILE' ) !== 'teacher' )
	{
		$mp_RET = DBGet( "SELECT MARKING_PERIOD_ID,TITLE,'" . (int) UserMPGroup() . "' AS GROUP_ID
			FROM school_marking_periods
			WHERE MP='QTR'
			AND SCHOOL_ID='" . UserSchool() . "'
			AND SYEAR='" . UserSyear() . "'" .
			MPGroupWhereSQL( UserMPGroup() ) .
			" ORDER BY SORT_ORDER IS NULL,SORT_ORDER,START_DATE" );
	}
	elseif ( User( 'PROFILE' ) === 'teacher' )
	{
		// Only get MP Groups for Teacher's Course Periods
		$cp_mp_RET = DBGet( "SELECT DISTINCT MARKING_PERIOD_ID
			FROM course_periods
			WHERE SYEAR='" . UserSyear() . "'
			AND SCHOOL_ID='" . UserSchool() . "'
			AND (TEACHER_ID='" . User( 'STAFF_ID' ) . "'
				OR SECONDARY_TEACHER_ID='" . User( 'STAFF_ID' ) . "')" );

		$group_ids = [];

		foreach ( (array) $cp_mp_RET as $cp_mp )
		{
			$group_ids[] = (int) GetMPGroup( $cp_mp['MARKING_PERIOD_ID'] );

			$group_ids = array_unique( $group_ids );
		}

		if ( in_array( 0, $group_ids ) )
		{
			$mp_RET = DBGet( "SELECT MARKING_PERIOD_ID,TITLE,'0' AS GROUP_ID
				FROM school_marking_periods
				WHERE MP='QTR'
				AND SCHOOL_ID='" . UserSchool() . "'
				AND SYEAR='" . UserSyear() . "'" .
				MPGroupWhereSQL( '0' ) .
				" ORDER BY SORT_ORDER IS NULL,SORT_ORDER,START_DATE" );

			$key = array_search( 0, $group_ids );

			unset( $group_ids[ $key ] );
		}

		if ( $group_ids )
		{
			$other_groups_RET = DBGet( "SELECT mp.MARKING_PERIOD_ID,mp.TITLE,mpg.GROUP_ID,
				(SELECT TITLE
					FROM school_marking_period_groups
					WHERE ID=mpg.GROUP_ID) AS GROUP_TITLE
				FROM school_marking_periods mp,school_marking_period_groupxmp mpg
				WHERE mp.MP='QTR'
				AND mp.SCHOOL_ID='" . UserSchool() . "'
				AND mp.SYEAR='" . UserSyear() . "'
				AND mp.MARKING_PERIOD_ID=mpg.MARKING_PERIOD_ID
				AND mpg.GROUP_ID IN(" . implode( ',', array_map( 'intval', $group_ids ) ) . ")
				ORDER BY mpg.GROUP_ID,mp.SORT_ORDER IS NULL,mp.SORT_ORDER,mp.START_DATE" );
		}
	}
	else
	{
		// Admin: all Groups
		$mp_RET = DBGet( "SELECT MARKING_PERIOD_ID,TITLE,'0' AS GROUP_ID
			FROM school_marking_periods
			WHERE MP='QTR'
			AND SCHOOL_ID='" . UserSchool() . "'
			AND SYEAR='" . UserSyear() . "'" .
			MPGroupWhereSQL( '0' ) .
			" ORDER BY SORT_ORDER IS NULL,SORT_ORDER,START_DATE" );

		$other_groups_RET = DBGet( "SELECT mp.MARKING_PERIOD_ID,mp.TITLE,mpg.GROUP_ID,
			(SELECT TITLE
				FROM school_marking_period_groups
				WHERE ID=mpg.GROUP_ID) AS GROUP_TITLE
			FROM school_marking_periods mp,school_marking_period_groupxmp mpg
			WHERE mp.MP='QTR'
			AND mp.SCHOOL_ID='" . UserSchool() . "'
			AND mp.SYEAR='" . UserSyear() . "'
			AND mp.MARKING_PERIOD_ID=mpg.MARKING_PERIOD_ID
			ORDER BY mpg.GROUP_ID,mp.SORT_ORDER IS NULL,mp.SORT_ORDER,mp.START_DATE" );
	}

	ob_start();

	?>
	<label for="mp" class="a11y-hidden"><?php echo _( 'Marking Period' ); ?></label>
	<select name="mp" id="mp" autocomplete="off" onChange="ajaxPostForm(this.form,true);">
	<?php if ( count( $mp_RET )
		|| count( $other_groups_RET ) ) :

		$mp_array = [];

		foreach ( (array) $mp_RET as $quarter ) : ?>
			<option value="<?php echo AttrEscape( $quarter['MARKING_PERIOD_ID'] ); ?>"<?php echo ( UserMP() == $quarter['MARKING_PERIOD_ID'] ? ' selected' : '' ); ?>><?php
				echo $quarter['TITLE'];
			?></option>
		<?php $mp_array[] = $quarter['MARKING_PERIOD_ID'];

		endforeach;

		$mp_group_tmp = '';

		foreach ( (array) $other_groups_RET as $quarter ) : ?>
			<?php if ( $mp_group_tmp !== $quarter['GROUP_TITLE'] ) :
				if ( $mp_group_tmp ) : ?>
					</optgroup>
				<?php endif;
				$mp_group_tmp = $quarter['GROUP_TITLE']; ?>
				<optgroup label="<?php echo AttrEscape( $quarter['GROUP_TITLE'] ); ?>">
			<?php endif; ?>
			<option value="<?php echo AttrEscape( $quarter['MARKING_PERIOD_ID'] ); ?>"<?php echo ( UserMP() == $quarter['MARKING_PERIOD_ID'] ? ' selected' : '' ); ?>><?php
				echo $quarter['TITLE'];
			?></option>
		<?php $mp_array[] = $quarter['MARKING_PERIOD_ID'];

		endforeach; ?>
			</optgroup>
		<?php

		// Reset UserMP if invalid.
		if ( ! UserMP()
			|| ! in_array( UserMP(), $mp_array ) ) :

			$_SESSION['UserMP'] = isset( $mp_RET[1]['MARKING_PERIOD_ID'] ) ?
				$mp_RET[1]['MARKING_PERIOD_ID'] : $other_groups_RET[1]['MARKING_PERIOD_ID'];

			// Reset UserMPGroup too.
			$_SESSION['UserMPGroup'] = isset( $mp_RET[1]['GROUP_ID'] ) ?
				$mp_RET[1]['GROUP_ID'] : $other_groups_RET[1]['GROUP_ID'];
		endif;

	// Error if no quarters.
	else : ?>

			<option value=""><?php
				echo _( 'Error' ) . ': ' . _( 'No quarters found' );
			?></option>

	<?php endif; ?>
	</select>
	<?php

	return ob_get_clean();
}
endif;


// Rollover Marking Period Groups to next school year.
add_action( 'School_Setup/Rollover.php|rollover_after', 'MarkingPeriodGroupsRollover' );

/**
 * Marking Period Groups Rollover
 *
 * Deletes any Marking Period Group referencing next school year.
 */
function MarkingPeriodGroupsRollover()
{
	if ( ! in_array( 'SCHOOL_MARKING_PERIODS', array_keys( $_REQUEST['tables'] ) ) // Compat with RosarioSIS 9.3-.
		&& ! in_array( 'school_marking_periods', array_keys( $_REQUEST['tables'] ) ) )
	{
		// Not Rolling Marking Periods, fail.
		return false;
	}

	$next_syear = UserSyear() + 1;

	// MARKING PERIOD GROUPS ROLLOVER.
	$delete_sql = "DELETE FROM school_marking_period_groupxmp
		WHERE GROUP_ID IN(SELECT ID
			FROM school_marking_period_groups
			WHERE SYEAR='" . $next_syear . "'
			AND SCHOOL_ID='" . UserSchool() . "');";

	$delete_sql .= "DELETE FROM school_marking_period_groups
		WHERE SYEAR='" . $next_syear . "'
		AND SCHOOL_ID='" . UserSchool() . "';";

	DBQuery( $delete_sql );

	DBQuery( "INSERT INTO school_marking_period_groups (TITLE,SCHOOL_ID,SYEAR,ROLLOVER_ID)
		SELECT TITLE,SCHOOL_ID,'" . $next_syear . "',ID
		FROM school_marking_period_groups mpg
		WHERE mpg.SYEAR='" . UserSyear() . "'
		AND mpg.SCHOOL_ID='" . UserSchool() . "'" );

	// Insert Groups cross Marking Periods references.
	DBQuery( "INSERT INTO school_marking_period_groupxmp (GROUP_ID,MARKING_PERIOD_ID)
		SELECT mpg.ID,mp.MARKING_PERIOD_ID
		FROM school_marking_period_groupxmp gmp,school_marking_period_groups mpg,school_marking_periods mp
		WHERE mpg.ROLLOVER_ID=gmp.GROUP_ID
		AND mp.ROLLOVER_ID=gmp.MARKING_PERIOD_ID
		AND mpg.SYEAR='" . $next_syear . "'
		AND mpg.SCHOOL_ID='" . UserSchool() . "'" );

	// Insert Groups cross Grade Levels references.
	DBQuery( "INSERT INTO school_marking_period_groupxgradelevel (GROUP_ID,GRADE_LEVEL_ID)
		SELECT mpg.ID,ggl.GRADE_LEVEL_ID
		FROM school_marking_period_groupxgradelevel ggl,school_marking_period_groups mpg
		WHERE mpg.ROLLOVER_ID=ggl.GROUP_ID
		AND mpg.SYEAR='" . $next_syear . "'
		AND mpg.SCHOOL_ID='" . UserSchool() . "'" );

	return true;
}
