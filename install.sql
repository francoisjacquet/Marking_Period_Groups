/**
 * Install PostgreSQL
 * Required if the module adds programs to other modules
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Marking Period Groups module
 */

-- Fix #102 error language "plpgsql" does not exist
-- http://timmurphy.org/2011/08/27/create-language-if-it-doesnt-exist-in-postgresql/
--
-- Name: create_language_plpgsql(); Type: FUNCTION; Schema: public; Owner: postgres
--
CREATE FUNCTION create_language_plpgsql()
RETURNS BOOLEAN AS $$
    CREATE LANGUAGE plpgsql;
    SELECT TRUE;
$$ LANGUAGE SQL;
SELECT CASE WHEN NOT (
    SELECT TRUE AS exists FROM pg_language
    WHERE lanname='plpgsql'
    UNION
    SELECT FALSE AS exists
    ORDER BY exists DESC
    LIMIT 1
) THEN
    create_language_plpgsql()
ELSE
    FALSE
END AS plpgsql_created;
DROP FUNCTION create_language_plpgsql();


/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Marking_Period_Groups/MarkingPeriods.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Marking_Period_Groups/MarkingPeriods.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Marking_Period_Groups/GradeLevels.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Marking_Period_Groups/GradeLevels.php'
    AND profile_id=1);


--
-- Name: school_marking_period_groups; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_school_marking_period_groups() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'school_marking_period_groups') THEN
    RAISE NOTICE 'Table "school_marking_period_groups" already exists.';
    ELSE
	    CREATE TABLE school_marking_period_groups (
            id serial PRIMARY KEY,
            syear numeric(4,0) NOT NULL,
            school_id integer NOT NULL,
            title text NOT NULL,
            rollover_id integer,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp,
            FOREIGN KEY (school_id,syear) REFERENCES schools(id,syear)
	    );
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_school_marking_period_groups();
DROP FUNCTION create_table_school_marking_period_groups();


--
-- Name: school_marking_period_groupxmp; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_school_marking_period_groupxmp() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'school_marking_period_groupxmp') THEN
    RAISE NOTICE 'Table "school_marking_period_groupxmp" already exists.';
    ELSE
        CREATE TABLE school_marking_period_groupxmp (
            group_id int NOT NULL REFERENCES school_marking_period_groups(id),
            marking_period_id int NOT NULL REFERENCES school_marking_periods(marking_period_id) ON DELETE CASCADE,
            UNIQUE (group_id, marking_period_id)
        );
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_school_marking_period_groupxmp();
DROP FUNCTION create_table_school_marking_period_groupxmp();


--
-- Name: school_marking_period_groupxgradelevel; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_school_marking_period_groupxgradelevel() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'school_marking_period_groupxgradelevel') THEN
    RAISE NOTICE 'Table "school_marking_period_groupxgradelevel" already exists.';
    ELSE
        CREATE TABLE school_marking_period_groupxgradelevel (
            group_id int NOT NULL REFERENCES school_marking_period_groups(id),
            grade_level_id int NOT NULL REFERENCES school_gradelevels(id) ON DELETE CASCADE,
            UNIQUE (group_id, grade_level_id)
        );
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_school_marking_period_groupxgradelevel();
DROP FUNCTION create_table_school_marking_period_groupxgradelevel();
