��          D      l       �      �      �      �   E   �     �           #     )  H   H                          Create a group Group Marking Period Groups Please copy the %s file and place it inside the functions/ directory. Project-Id-Version: Marking Period Groups module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:17+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Crear un grupo Grupo Grupos de periodos a calificar Por favor copiar el archivo %s y ponerlo dentro de la carpeta functions/ 