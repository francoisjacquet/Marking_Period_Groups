��          D      l       �      �      �      �   E   �   �  �      �            M   ,                          Create a group Group Marking Period Groups Please copy the %s file and place it inside the functions/ directory. Project-Id-Version: Marking Period Groups module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:18+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Créer un groupe Groupe Groupes de périodes scolaires Merci de copier le fichier %s et de le placer dans le répertoire functions/. 