Marking Period Groups module
============================

![screenshot](https://gitlab.com/francoisjacquet/Marking_Period_Groups/raw/master/screenshot.png?inline=false)

Version 1.2 - August, 2024

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by @Hirama666, Lybia

DESCRIPTION
-----------
This module provides a way to add groups of Marking Periods. This can be useful if you have, for example, some Grade Levels with 3 semesters, and others with only 2.

The recommended solution without this module is to create various schools, one for each of the Grade Levels with the same Marking Periods. See warning below before installing this module.

With this module, you can create additional groups of Marking Periods, with their own dates using the new _Marking Periods_ program. It is recommended to deactivate the core _Marking Periods_ program as it does not handle groups (this for all _User Profiles_).

The new _Grade Levels_ program lets you assign a group to each Grade Level.

The "Default" group is the set of existing Marking Periods prior to the module installation. It cannot be deleted.

The Quarters dropdown in the left menu will display quarters for all groups.

For Parents, and Students, the Quarters dropdown will be for the group associated with the student's Grade Level.

Translated in French & Spanish.


### Warning

This module can be considered as experimental. Here are known limitations.

A student is associated to a Marking Period group via its Grade Level (see the new _Grade Levels_ program). That being said, there is no safety measure to prevent the scheduling of a student to Course Periods associated to another group. Behavior in this case should be error prone.

Please note the most impacted module is _Scheduling_:

- Courses
- Group Schedule
- Group Drops
- Print Schedules

The above programs all have a "Marking Period" dropdown. This dropdown will list Marking Periods for the current group only. That is, the selected marking period group (left menu).

- Print Class Lists
- Print Class Pictures

The above programs are limited. Students will not be found if their class do not belong to the selected marking period group (left menu), unless you check "Include Inactive Students".

The following add-ons will not work properly:

- Class Diary Premium: email reminders will be sent for one course periods in one marking period group only.
- Email Alerts: alerts will be sent for the current marking period in one group only.
- Attendance Excel Sheet: students will not be found if their class do not belong to the selected marking period group (left menu), unless you chack "Include Inactive Students".
- Semester Rollover: start date for Semester 2 may be wrong depending on student's Grade Level.
- Automatic Attendance: daily attendance may not be calculated.

Finally, if you use the _School > Copy School_ program, please make sure to uncheck "Marking Periods".

### Deactivate

When deactivating the module, you should also manually delete the core `functions/1GetMP.php` file.

### Delete

You can run this SQL query to delete the extra Marking Periods. It will produce an error if one of the Marking Periods has dependencies (attributed to Course Periods, etc).

```sql
DELETE FROM school_marking_periods
WHERE MARKING_PERIOD_ID IN(SELECT MARKING_PERIOD_ID
	FROM school_marking_period_groupxmp);
```

CONTENT
-------
School
- Marking Periods
- Grade Levels

INSTALL
-------
Copy the `Marking_Period_Groups/` folder (if named `Marking_Period_Groups-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 11.1+
