<?php
/**
 * Marking Period Groups module Menu entries
 *
 * @uses $menu global var
 *
 * @package Marking Period Groups module
 *
 * @see  Menu.php in root folder
 */

$menu['School_Setup']['admin'][] = dgettext( 'Marking_Period_Groups', 'Marking Period Groups' );
$menu['School_Setup']['admin'] += [
	'Marking_Period_Groups/MarkingPeriods.php' => _( 'Marking Periods' ),
	'Marking_Period_Groups/GradeLevels.php' => _( 'Grade Levels' ),
];

$menu['School_Setup']['teacher'][] = dgettext( 'Marking_Period_Groups', 'Marking Period Groups' );
$menu['School_Setup']['teacher']['Marking_Period_Groups/MarkingPeriods.php'] = _( 'Marking Periods' );

$menu['School_Setup']['parent'][] = dgettext( 'Marking_Period_Groups', 'Marking Period Groups' );
$menu['School_Setup']['parent']['Marking_Period_Groups/MarkingPeriods.php'] = _( 'Marking Periods' );
